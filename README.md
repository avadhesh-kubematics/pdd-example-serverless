# pdd-example

Read a list of account IDs from each of two files, and combine them.

Design:
https://drive.google.com/open?id=1B9ZQ8Nn-4JMaqOR3dkf3tTDHS_yeepHciVqmu-qZrbs

![](PDDExample.svg)
